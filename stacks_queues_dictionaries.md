#Script

### FIRST:

Idea: discuss why you'd use the 'deque' class from collections as opposed to a list. Afterwards, show how to do the same by wrapping it in a class.
Speak: "Python lists are dynamically allocated structures which means that if you have, say 10 items on them in a contiguous part of memory and you run out of space while trying to add an 11th item, then it will look
for an area with 10*2 blocks of memory, then copy the 10 items in the original blocks of memory to the new blocks and only then, finally add the 11th item. This is a very costly operation. So for these reasons we prefer the deque implementations of stacks."

